/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.fileserver.client.net;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import se.kth.id1212.fbirwe.fileserver.common.Constants;

/**
 *
 * @author fredericbirwe
 */
public class FileWriter {
    Socket client;
    private final static int PORT = Constants.PORT;
    private final static String IP = Constants.IP;
    
    private DataOutputStream toServer;
    
    public FileWriter() {        
        try {
            client = new Socket( IP, PORT );

            toServer = new DataOutputStream(client.getOutputStream());
                        
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }   
    
    
    public void disconnect() {
        try {
            client.close();
        } catch (IOException ex) {
            Logger.getLogger(ClientNet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public void sendFile(String filepath) throws IOException {
        File file = new File( filepath );
                
        if( file.isFile() ) {
            FileInputStream fileInputStream = null;
            byte[] bFile = new byte[(int) file.length()];
            try
            {
               //convert file into array of bytes
               fileInputStream = new FileInputStream(file);
               fileInputStream.read(bFile);
               fileInputStream.close();
               
               toServer.writeUTF( file.getName() );
               toServer.writeInt( bFile.length );
               toServer.write( bFile );
               toServer.flush();
                              
               disconnect();
            }
            catch (Exception e)
            {
               e.printStackTrace();
            }
        }
    }
}
