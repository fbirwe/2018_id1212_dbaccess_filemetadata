/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.fileserver.server.model;

import java.util.StringJoiner;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.LockModeType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Version;
import se.kth.id1212.fbirwe.fileserver.common.FileMessage;

/**
 *
 * @author fredericbirwe
 */
@NamedQueries({
        @NamedQuery(
            name = "getFileByName",
            query = "SELECT file FROM File file WHERE file.filename LIKE :filename",
            lockMode = LockModeType.OPTIMISTIC
    )
    ,
        @NamedQuery(
            name = "getAllFiles",
            query = "SELECT file FROM File file",
            lockMode = LockModeType.OPTIMISTIC
    )
})

@Entity(name = "File")
public class File implements FileMessage{
    @Id
    @Column(name = "filename", nullable = false)    
    private String filename;
    
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "owner", nullable = false)
    private User owner;
    
    @Column(name = "publicFile", nullable = false)
    private boolean publicFile;
    
    @Version
    @Column(name = "OPTLOCK")
    private int versionNum;
    
    public File(String filename, User owner, boolean publicFile) {
        this.filename = filename;
        this.owner = owner;
        this.publicFile = publicFile;
    }
    
    public File() {
        this.filename = null;
        this.owner = null;
        this.publicFile = false;
    }

    public String getFilename() {
        return filename;
    }

    public String getOwnerName() {
        return getOwner().getUsername();
    }
    
    public User getOwner() {
        return owner;
    }

    public boolean isPublicFile() {
        return publicFile;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public void setPublicFile(boolean publicFile) {
        this.publicFile = publicFile;
    }
    
    public String toString() {
        StringJoiner jnr = new StringJoiner(", ");
        jnr.add(this.filename);
        jnr.add(this.owner.getUsername());
        jnr.add(this.publicFile ? "public file" : "private file");
        
        return jnr.toString();
    }
}
