/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.fileserver.server.startup;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.logging.Level;
import java.util.logging.Logger;
import se.kth.id1212.fbirwe.fileserver.common.Fileserver;
import se.kth.id1212.fbirwe.fileserver.common.UserAlreadyRegisteredException;
import se.kth.id1212.fbirwe.fileserver.server.controller.Controller;
import se.kth.id1212.fbirwe.fileserver.server.model.User;

/**
 *
 * @author fredericbirwe
 */
public class Server {
    private String fileserverName = Fileserver.REGISTRY_NAME;
    public final static String DATA_DIR = "/Users/fredericbirwe/Desktop/Uni_7Semester_KTH/Network Programming/Homework/homework_03/db_dir";

    public static void main(String[] args) {
        try {
            Server server = new Server();
            server.startRMIServant();
            System.out.println("Fileserver started.");
        } catch (RemoteException | MalformedURLException e) {
            e.printStackTrace();
            System.out.println("Failed to start fileserver.");
        }
    }

    private void startRMIServant() throws RemoteException, MalformedURLException {
        try {
            LocateRegistry.getRegistry().list();
        } catch (RemoteException noRegistryRunning) {
            LocateRegistry.createRegistry(Registry.REGISTRY_PORT);
        }
        Controller contr = new Controller();
        Naming.rebind(fileserverName, contr);        
    }
}
