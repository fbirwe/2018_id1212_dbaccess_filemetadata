/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.fileserver.client.net;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import se.kth.id1212.fbirwe.fileserver.common.Constants;

/**
 *
 * @author fredericbirwe
 */
public class ClientNet {
    Socket client;
    private final static int PORT = Constants.PORT;
    private final static String IP = Constants.IP;
    
    private DataInputStream fromServer;
    private DataOutputStream toServer;
    private Scanner sc;
    //private OutputHandler outputHandler;
    //private Listener listener;
    
    public ClientNet(/*OutputHandler outputHandler*/) {
        //this.outputHandler = outputHandler;
        
        try {
            client = new Socket( IP, PORT );

            toServer = new DataOutputStream(client.getOutputStream());
            fromServer = new DataInputStream(client.getInputStream());
            //listener = new Listener();
            
            //new Thread(listener).start();
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        

    }   
    
    
    public void disconnect() {
        try {
            client.close();
        } catch (IOException ex) {
            Logger.getLogger(ClientNet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public void sendFile(String filepath) throws IOException {
        File file = new File( filepath );
                
        FileInputStream fileInputStream = null;
        byte[] bFile = new byte[(int) file.length()];
        try
        {
           //convert file into array of bytes
           fileInputStream = new FileInputStream(file);
           fileInputStream.read(bFile);
           fileInputStream.close();
           
           toServer.writeUTF( file.getName() );
           toServer.writeInt( bFile.length );
           toServer.write( bFile );
           toServer.flush();
        }
        catch (Exception e)
        {
           e.printStackTrace();
        }
    }
    
    /*private class Listener implements Runnable {
        @Override
        public void run() {
                outputHandler.handleMsg("Connection established to: " + IP + ":" + PORT );
                outputHandler.handleMsg( Texts.WELCOME );
                outputHandler.handleMsg( Texts.RULES );
                
                try {
                    while(true) {                   
                        Message incoming = (Message) fromServer.readObject();                        
                        
                        if(incoming.getSize() == Helper.sizeof(incoming)) {
                        
                            // Print received Message
                            if(incoming.getType() == MsgType.STATUS) {                                
                                outputHandler.handleMsg(Texts.showStatus((StatusMessage) incoming));

                            } else if (incoming.getType() == MsgType.GAME_WON || incoming.getType() == MsgType.GAME_LOST) {
                                Message restart;

                                outputHandler.setInputState(InputState.RETRY);
                                outputHandler.handleMsg(incoming.getType() == MsgType.GAME_WON ? Texts.GAME_WON : Texts.GAME_LOST);
                                outputHandler.handleMsg( Texts.showSolution( (GameEndMessage) incoming ) );
                                outputHandler.handleMsg(Texts.showScore((GameEndMessage) incoming));
                                outputHandler.handleMsg( Texts.TRY_AGAIN );
                            }
                        }
                    }
                } catch (Exception ex) {
                    Logger.getLogger(ClientNet.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    disconnect();
                }

        }
    }*/
}
