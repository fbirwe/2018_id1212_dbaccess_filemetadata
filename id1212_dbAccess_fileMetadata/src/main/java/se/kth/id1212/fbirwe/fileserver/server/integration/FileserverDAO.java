/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.fileserver.server.integration;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import se.kth.id1212.fbirwe.fileserver.server.model.File;
import se.kth.id1212.fbirwe.fileserver.server.model.User;

/**
 *
 * @author fredericbirwe
 */
public class FileserverDAO {
    private final EntityManagerFactory emFactory;
    private final ThreadLocal<EntityManager> threadLocalEntityManager = new ThreadLocal<>();
    
    public FileserverDAO() {
        emFactory = Persistence.createEntityManagerFactory("fileserverPersistenceUnit");
    }
    
    private EntityManager beginTransaction() {
        EntityManager em = emFactory.createEntityManager();
        threadLocalEntityManager.set(em);
        EntityTransaction transaction = em.getTransaction();
        if (!transaction.isActive()) {
            transaction.begin();
        }
        return em;
    }
    
    private void commitTransaction() {
        threadLocalEntityManager.get().getTransaction().commit();
    }
    
    /* methods regarding to Users */
    public void addUser(User user) {
        try {
            EntityManager em = beginTransaction();
            em.persist(user);
        } finally {
            commitTransaction();
        }
    }
    
    public User getUser( String name ) {
        try {
            EntityManager em = beginTransaction();
            try {
                return em.createNamedQuery("getUserByName", User.class).setParameter("username", name).getSingleResult();
            } catch (NoResultException noSuchUser) {
                return null;
            }
        } finally {
            commitTransaction();
        }
    }
    
    public List<User> getAllUsers() {
        try {
            EntityManager em = beginTransaction();
            try {
                return em.createNamedQuery("getAllUsers", User.class).getResultList();
            } catch (NoResultException noSuchUser) {
                return new ArrayList<>();
            }
        } finally {
            commitTransaction();
        }
    }
    
    public void addFile( String filename, String ownerName, boolean publicFile ) {                  
        try {
            EntityManager em = beginTransaction();
            User owner = em.getReference(User.class, ownerName);

            System.out.println( owner.toString() );

            if( owner != null) {
                em.persist( new File(filename, owner, publicFile) );
            }
        } finally {
            commitTransaction();
        }
    }
    
    public File getFile( String name ) {
        try {
            EntityManager em = beginTransaction();
            try {
                return em.createNamedQuery("getFileByName", File.class).setParameter("filename", name).getSingleResult();
            } catch (NoResultException noSuchUser) {
                return null;
            }
        } finally {
            commitTransaction();
        }
    }
    
    public List<File> getAllFiles() {
    try {
            EntityManager em = beginTransaction();
            try {
                return em.createNamedQuery("getAllFiles", File.class).getResultList();
            } catch (NoResultException noSuchUser) {
                return new ArrayList<>();
            }
        } finally {
            commitTransaction();
        }
    }
}