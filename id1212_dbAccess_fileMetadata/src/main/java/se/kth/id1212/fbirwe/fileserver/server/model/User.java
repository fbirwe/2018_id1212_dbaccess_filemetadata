/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.fileserver.server.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.LockModeType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Version;
import se.kth.id1212.fbirwe.fileserver.common.UserMessage;


/**
 *
 * @author fredericbirwe
 */


@NamedQueries({
        @NamedQuery(
            name = "getUserByName",
            query = "SELECT usr FROM registered_user usr WHERE usr.username LIKE :username",
            lockMode = LockModeType.OPTIMISTIC
    )
    ,
        @NamedQuery(
            name = "getAllUsers",
            query = "SELECT usr FROM registered_user usr",
            lockMode = LockModeType.OPTIMISTIC
    )
})

@Entity(name = "registered_user")
public class User implements UserMessage {

    @Id
    @Column(name = "username", nullable = false)
    private String username;
    
    @Column(name = "password", nullable = false)
    private String password;
    
    @Version
    @Column(name = "OPTLOCK")
    private int versionNum;
    
    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }
    
    public User() {
        this.username = null;
        this.password = null;
    }
    
    public String getPassword() {
        return this.password;
    }
    
    public String getUsername() {
        return this.username;
    }
    
    public boolean equals( User usr ) {
        if( usr == null ) {
            return false;
        } else {
            return this.getUsername().equals( usr.getUsername() ) && this.getPassword().equals( usr.getPassword() );
        }
    }
    
    public String toString() {
        return "username: " + this.username + ", password: " + this.password;
    }
}
