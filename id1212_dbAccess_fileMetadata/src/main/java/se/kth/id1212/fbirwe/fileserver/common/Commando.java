/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.fileserver.common;

/**
 *
 * @author fredericbirwe
 */
public enum Commando {
    HELP,
    SIGN_UP,
    LOGIN,
    LOGOUT,
    LIST_FILES,
    LIST_USERS,
    RETRY,
    READ_FILE,
    SET_DIR,
    WRITE_FILE
}
