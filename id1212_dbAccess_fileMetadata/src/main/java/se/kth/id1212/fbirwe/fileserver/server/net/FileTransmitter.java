/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.fileserver.server.net;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import se.kth.id1212.fbirwe.fileserver.common.Constants;

/**
 *
 * @author fredericbirwe
 */
public class FileTransmitter implements Runnable {
    private static final int PORT = Constants.PORT;
    private static final int LINGER_TIME = 5000;
    private static final int TIMEOUT = 600000;
    private ArrayList<ClientHandler> clients = new ArrayList<>();
    
    public void run() {
        
        try {
            ServerSocket listeningSocket = new ServerSocket(PORT);
            System.out.println("Server listening on port " + PORT);
            while (true) {
                Socket clientSocket = listeningSocket.accept();
                System.out.println("Client received");
                startHandler(clientSocket);
            }
        } catch (IOException e) {
            System.err.println("Server failure.");
        }
    }
    
    private void startHandler(Socket clientSocket) throws SocketException {
        clientSocket.setSoLinger(true, LINGER_TIME);
        clientSocket.setSoTimeout(TIMEOUT);
        ClientHandler handler = new ClientHandler(this, clientSocket);
        synchronized (clients) {
            clients.add(handler);
        }
        Thread handlerThread = new Thread(handler);
        handlerThread.setPriority(Thread.MAX_PRIORITY);
        handlerThread.start();
    }
    
    public void removeHandler(ClientHandler handler) {
        clients.remove(handler);
    }

}
