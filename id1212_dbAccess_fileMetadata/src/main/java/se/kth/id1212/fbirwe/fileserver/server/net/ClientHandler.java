package se.kth.id1212.fbirwe.fileserver.server.net;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.UncheckedIOException;
import java.net.Socket;
import java.util.StringJoiner;
import java.util.concurrent.CompletableFuture;
import java.util.logging.Level;
import java.util.logging.Logger;
import se.kth.id1212.fbirwe.fileserver.common.Constants;
import se.kth.id1212.fbirwe.fileserver.server.startup.Server;

/**
 *
 * @author fredericbirwe
 */
public class ClientHandler implements Runnable {
    private FileTransmitter server;
    private Socket clientSocket;    
    private DataInputStream fromClient;
    private DataOutputStream toClient;
    
    public ClientHandler(FileTransmitter server, Socket clientSocket) {
        this.server = server;
        this.clientSocket = clientSocket;        
    }

    @Override
    public void run() {
        try {
            fromClient = new DataInputStream(clientSocket.getInputStream());
            toClient = new DataOutputStream(clientSocket.getOutputStream());
            
            while( true ) {
                
                String firstStatement = fromClient.readUTF();
                
                System.out.println( firstStatement );
                
                if( firstStatement.contains( Constants.REQUIRE_PREFACE ) ) {
                    String filename = firstStatement.replace( Constants.REQUIRE_PREFACE, "");
                    
                    sendFile( filename );
                    
                } else {
                    String filename = firstStatement;

                    int size = fromClient.readInt();
                    byte[] file = new byte[size];
                    int readSize = fromClient.read( file );

                    if( size == readSize ) {
                        writeFile( filename, file );
                    }
                }
                

            }
            
        } catch (IOException ex) {
            Logger.getLogger(ClientHandler.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }
    
    private void sendFile( String filename ) throws IOException {
        StringJoiner filePath = new StringJoiner( System.getProperty("os.name").toLowerCase().contains("win") ? "\\" : "/" );
        filePath.add( Server.DATA_DIR );
        filePath.add( filename );
        
        File file = new File( filePath.toString() );
        
        System.out.println( filePath.toString() );
        
        if( file.isFile() ) {
            FileInputStream fileInputStream = null;
            byte[] bFile = new byte[(int) file.length()];
            try
            {
               //convert file into array of bytes
               fileInputStream = new FileInputStream(file);
               fileInputStream.read(bFile);
               fileInputStream.close();

               toClient.writeUTF( file.getName() );
               toClient.writeInt( bFile.length );
               toClient.write( bFile );
               toClient.flush();
                              
            }
            catch (Exception e)
            {
               e.printStackTrace();
            }
        }
    }
    
    private void endSession() {
        System.out.println("Socket closed");
        try {
            this.clientSocket.close();
        } catch (IOException ex) {
            Logger.getLogger(ClientHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        server.removeHandler(this);
        
    }
    
    private void writeFile( String filename, byte[] file ) {
        CompletableFuture.runAsync(() -> {
            FileOutputStream outputStream = null;
            try {
                StringJoiner filePath = new StringJoiner( System.getProperty("os.name").toLowerCase().contains("win") ? "\\" : "/" );
                filePath.add( Server.DATA_DIR );
                filePath.add( filename );
                
                outputStream = new FileOutputStream( filePath.toString() );
                DataOutputStream dataOutStream = new DataOutputStream(new BufferedOutputStream(outputStream));
                dataOutStream.write(file, 0, file.length );
                dataOutStream.close();  
                
            } catch (FileNotFoundException ex) {
                Logger.getLogger(ClientHandler.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ClientHandler.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    outputStream.close();
                } catch (IOException ex) {
                    Logger.getLogger(ClientHandler.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }
    
}
