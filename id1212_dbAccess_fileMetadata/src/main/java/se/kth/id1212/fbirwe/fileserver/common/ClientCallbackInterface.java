/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.fileserver.common;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *
 * @author fredericbirwe
 */
public interface ClientCallbackInterface extends Remote {
    
    public static final String NAME = "ClientCallbackInterface";

    void notifyClient( NotificationType type, FileMessage changedFile, UserMessage changingUser) throws RemoteException;

}
