/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.fileserver.client.view;

import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Scanner;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.logging.Level;
import java.util.logging.Logger;
import se.kth.id1212.fbirwe.fileserver.client.net.FileReader;
import se.kth.id1212.fbirwe.fileserver.client.net.FileWriter;
import se.kth.id1212.fbirwe.fileserver.common.AccessDeniedException;
import se.kth.id1212.fbirwe.fileserver.common.ClientCallbackInterface;
import se.kth.id1212.fbirwe.fileserver.common.Commando;
import se.kth.id1212.fbirwe.fileserver.common.FileMessage;
import se.kth.id1212.fbirwe.fileserver.common.FileNotFoundException;
import se.kth.id1212.fbirwe.fileserver.common.Fileserver;
import se.kth.id1212.fbirwe.fileserver.common.NotLoggedInException;
import se.kth.id1212.fbirwe.fileserver.common.NotificationType;
import se.kth.id1212.fbirwe.fileserver.common.UserAlreadyLoggedInException;
import se.kth.id1212.fbirwe.fileserver.common.UserAlreadyRegisteredException;
import se.kth.id1212.fbirwe.fileserver.common.UserMessage;
import se.kth.id1212.fbirwe.fileserver.server.model.User;

/**
 *
 * @author fredericbirwe
 */
public class FileserverClientView implements Runnable {
    private Scanner sc;
    private Fileserver server;
    private UUID clientId;
    private String loggedInAs;
    private NotificationListener listener;
    private String clientDir = "";
    
    public static void main(String[] args) {
        new FileserverClientView();
    }
    
    public FileserverClientView() {
        try {
            this.sc = new Scanner(System.in);
            this.server = (Fileserver) Naming.lookup(Fileserver.REGISTRY_NAME);           
            
            new Thread(this).start();
            
        } catch ( NotBoundException | MalformedURLException | RemoteException ex) {
            ex.printStackTrace();
        }
    }
    
    @Override
    public void run() {
        safePrintln( Texts.WELCOME );
        
        while( true ) {
            String[] input = sc.nextLine().split(" ");
            Commando co = receiveCommando( input );
            
            try {               
                switch (co) {
                    case HELP :
                        safePrintln( Texts.HELP );
                        break;
                    case SIGN_UP :
                        this.server.createUser( input[1], input[2] );
                        break;
                    case LIST_FILES :
                        for( FileMessage f : this.server.getAllFiles( this.clientId ) ) {
                            safePrintln( f.toString() );
                        }
                        break;
                    case LIST_USERS :
                        for( String u : this.server.getAllUsers( this.clientId ) ) {
                           safePrintln(u);
                        }
                        break;
                    case LOGIN :
                        UUID identifier;
                        
                        try {
                            identifier = this.server.login( input[1], input[2], new NotificationListener() );
                   
                            if ( identifier != null ) {
                                clientId = identifier;
                                loggedInAs = input[1];
                                safePrintln(Texts.LOGIN_SUCCESSFULL( loggedInAs ));
                            } else {
                                safePrintln(Texts.LOGIN_FAILED);
                            }
                        } catch (IOException | NotBoundException ex) {
                            Logger.getLogger(FileserverClientView.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        break;
                    case LOGOUT : 
                        this.loggedInAs = "";
                        this.server.logout(clientId);
                        
                        safePrintln(Texts.LOGOUT_SUCCESSFULL);
                        break;
                    case READ_FILE :
                        if( !this.clientDir.equals("") ) {
                            this.server.readFile( this.clientId, input[1] );
                            new Thread(new FileReader( input[1], this.clientDir )).start();                            
                        } else {
                            safePrintln( Texts.DIR_NOT_SET );
                        }
                        

                        break;
                    case SET_DIR :
                        if( input.length >= 2 ) {
                            this.clientDir = input[2];
                        }
                        break;
                    case WRITE_FILE :
                        if( input.length > 1 ) {
                            boolean publicFile = false;

                            if(input.length > 2) {
                                publicFile = yesOrNo(input[2]);
                            }

                            this.writeFile( input[1], publicFile );
                        }
                        break;
                    default :
                        safePrintln( Texts.RETRY );
                }
                
                

            } catch (RemoteException ex ) {
                ex.printStackTrace();
            } catch ( UserAlreadyRegisteredException ex ) {
                safePrintln( Texts.USER_ALREADY_REGISTERED );
            } catch ( NotLoggedInException ex ) {
                safePrintln( Texts.NOT_LOGGED_IN );
            } catch (UserAlreadyLoggedInException ex) {
                safePrintln( Texts.USER_ALREADY_LOGGED_IN );
            } catch (FileNotFoundException ex) {
                safePrintln( Texts.FILE_NOT_FOUND );
            }
        }
    }
    
    private boolean yesOrNo( String input ) {
        input = input.toLowerCase().trim();
        
        return input.equals("yes") || input.equals("y");
    }
    
    private boolean isLoggedIn() {
        return !this.loggedInAs.equals("");
    }
        
    private Commando receiveCommando( String[] input ) {        
        if( input[0].matches("--help") || input[0].matches("-h") ) {
            return Commando.HELP;
        }
        
        if( input[0].matches("--sign_up") || input[0].matches("-su") ) {
            return Commando.SIGN_UP;
        }
        
        if( input[0].matches("--login") || input[0].matches("-li") ) {
            return Commando.LOGIN;
        }
        
        if( input[0].matches("--logout") || input[0].matches("-lo") ) {
            return Commando.LOGOUT;
        }
        
        if( input[0].matches("--list_users") || input[0].matches("-lu") ) {
            return Commando.LIST_USERS;
        }
        
        if( input[0].matches("--list_files") || input[0].matches("-ls") ) {
            return Commando.LIST_FILES;
        }

        if( input[0].matches("--set_dir") || input[0].matches("-sd") ) {
            return Commando.SET_DIR;
        }
        
        if( input[0].matches("--write_file") || input[0].matches("-wf") ) {
            return Commando.WRITE_FILE;
        }
        
        if( input[0].matches("--read_file") || input[0].matches("-rf") ) {
            return Commando.READ_FILE;
        }
        
        return Commando.RETRY;
    }
    
    private synchronized void safePrint( String s ) {
        System.out.print(s);
    }

    private synchronized void safePrintln ( String s ) {
        System.out.println(s);
    }
    
    private void writeFile( String filepath, boolean isPublicFile ){
        
        CompletableFuture.runAsync(() -> {            
            try {
                String[] splittedFilePath = filepath.split("/");              
                String filename = splittedFilePath[ splittedFilePath.length - 1 ];
               
                try {
                    this.server.writeFile(  this.clientId, filename, this.loggedInAs , isPublicFile);
                    new FileWriter().sendFile(filepath);
                } catch (AccessDeniedException ex) {
                    safePrintln( Texts.ACCESS_DENIED );
                }

                safePrintln("File written");
                
            } catch (NotLoggedInException | RemoteException ex) {
                ex.printStackTrace();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            
        }  );
    }
        
    private class NotificationListener extends UnicastRemoteObject implements ClientCallbackInterface  {

        public NotificationListener() throws IOException, NotBoundException, RemoteException {
        }
        
        @Override
        public void notifyClient(NotificationType type, FileMessage changedFile, UserMessage changingUser) throws RemoteException {
            if( type == NotificationType.READ_FILE ) {
                safePrintln( Texts.NOTIFY_FILE_READ( changedFile, changingUser ));
            } else {
                    safePrintln( Texts.NOTIFY_FILE_WRITE( changedFile, changingUser ));
            }
        }
    }
    
}
