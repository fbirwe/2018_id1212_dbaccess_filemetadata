/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.fileserver.common;

/**
 *
 * @author fredericbirwe
 */
public class NotLoggedInException extends Exception {
    public NotLoggedInException() {
        super("The user is not logged in");
    }
}
