/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.fileserver.common;

import java.io.Serializable;

/**
 *
 * @author fredericbirwe
 */
public interface UserMessage extends Serializable {
    public String getUsername();
}
