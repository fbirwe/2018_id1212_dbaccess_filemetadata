/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.fileserver.common;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;
import java.util.UUID;
import se.kth.id1212.fbirwe.fileserver.server.model.User;
import se.kth.id1212.fbirwe.fileserver.client.view.FileserverClientView;

/**
 *
 * @author fredericbirwe
 */
public interface Fileserver extends Remote {
    public static final String REGISTRY_NAME = "fileserver";
    
    /**
     *
     * @param username
     * @param password
     * @throws UserAlreadyRegisteredException
     */
    public void createUser(String username, String password) throws UserAlreadyRegisteredException, RemoteException;
    
    
    public List<String> getAllUsers( UUID userId ) throws NotLoggedInException, RemoteException;
    public List<? extends FileMessage> getAllFiles( UUID userId ) throws NotLoggedInException, RemoteException;
    public UUID login( String username, String pw, ClientCallbackInterface listener ) throws UserAlreadyLoggedInException, RemoteException;
    public void logout( UUID userId ) throws NotLoggedInException, RemoteException;
    public void writeFile(  UUID userId, String filename, String owner, boolean publicFile ) throws AccessDeniedException, NotLoggedInException, RemoteException;
    public FileMessage readFile(  UUID userId, String filename ) throws FileNotFoundException, NotLoggedInException, RemoteException;
}
