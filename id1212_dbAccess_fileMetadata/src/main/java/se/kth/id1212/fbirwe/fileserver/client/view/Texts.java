/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.fileserver.client.view;

import se.kth.id1212.fbirwe.fileserver.common.FileMessage;
import se.kth.id1212.fbirwe.fileserver.common.UserMessage;

/**
 *
 * @author fredericbirwe
 */
public class Texts {
    // Strings
    public static String ACCESS_DENIED = "You are not allowed to write this file";
    public static String DIR_NOT_SET = "You have not specified a download directory. Please set a dir using -sd or --set_dir <dirpath>.";
    public static String FILE_NOT_FOUND = "The requested file was not found.";
    public static String HELP = "possible commands:\n"
            + "--help or -h:                                shows help.\n"
            + "--sign_up or -su <username> <password>:      creates a new user with the given username and password.\n"
            + "--login or -li <username> <password>:        login as the specified user.\n"
            + "--logout or -lo:                             logs the user out.\n"
            + "--list_users or -lu:                         lists all users that are registered at the file server.\n"
            + "--list_files or -ls:                         lists all files that are stored at the server.\n"
            + "--set_dir or -sd <dirpath>:                  sets the download directory for downloading files.\n"
            + "--write_file or -wf <filepath> <isPublic>:   writes a new file or updates an existing file specified by the path and uploads it to the server. Type yes or y as second parameter to make the file public writeable.\n"
            + "--read_file or -rf <filename>:               reads the specified file";
    public static String LOGIN_FAILED = "The Login was not possible. The username or the password were wrong.";
    public static String LOGOUT_SUCCESSFULL = "Logout successfull. Good bye!";
    public static String NOT_LOGGED_IN = "You need to be logged in for this action.";
    public static String RETRY = "This method is not known. Please try again or use --help for help";
    public static String USER_ALREADY_LOGGED_IN = "This user is already logged in.";
    public static String USER_ALREADY_REGISTERED = "This username is already used. Choose another one.";
    public static String WELCOME = "Welcome! You are not logged in yet.\n"
            + "For logging in type in -li <username> <password>.\n"
            + "If you are have signed up yet, type in -su <username> <password>.\n"
            + "For a list of possible commands, type -h or --help";
    
    // Methods
    public static String LOGIN_SUCCESSFULL( String username ) {
        return "The Login was succesfull.\n"
                + "Welcome " + username + "!";
    }
    public static String NOTIFY_FILE_READ( FileMessage changedFile, UserMessage changingUser ) {
        return "The file " + changedFile.getFilename() + " was read by " + changingUser.getUsername();
    }
    public static String NOTIFY_FILE_WRITE( FileMessage changedFile, UserMessage changingUser ) {
        return "The file " + changedFile.getFilename() + " was updated by " + changingUser.getUsername();
    }
    
}
