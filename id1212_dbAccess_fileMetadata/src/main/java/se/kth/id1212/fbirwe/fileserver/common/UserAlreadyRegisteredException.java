/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.fileserver.common;

/**
 *
 * @author fredericbirwe
 */
public class UserAlreadyRegisteredException extends Exception {
    public UserAlreadyRegisteredException() {
        super("a user with the same username is already registered");
    }
}
