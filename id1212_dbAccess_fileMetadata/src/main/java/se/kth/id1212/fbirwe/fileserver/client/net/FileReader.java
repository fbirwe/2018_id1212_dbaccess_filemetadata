/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.fileserver.client.net;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;
import java.util.StringJoiner;
import java.util.concurrent.CompletableFuture;
import java.util.logging.Level;
import java.util.logging.Logger;
import se.kth.id1212.fbirwe.fileserver.client.view.FileserverClientView;
import se.kth.id1212.fbirwe.fileserver.common.Constants;
import se.kth.id1212.fbirwe.fileserver.server.net.ClientHandler;
import se.kth.id1212.fbirwe.fileserver.server.startup.Server;

/**
 *
 * @author fredericbirwe
 */
public class FileReader implements Runnable {
    Socket client;
    private final static int PORT = Constants.PORT;
    private final static String IP = Constants.IP;
    private final static String USER_DIR = Constants.USER_DIR;
    
    private DataInputStream fromServer;
    private DataOutputStream toServer;
    private String filename;
    private String clientDir;
    
    public FileReader( String filename, String clientDir ) {        
        try {
            client = new Socket( IP, PORT );

            fromServer = new DataInputStream(client.getInputStream());
            toServer = new DataOutputStream(client.getOutputStream());
            this.filename = filename;
            this.clientDir = clientDir;
                        
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }   
    
    
    public void disconnect() {
        try {
            client.close();
        } catch (IOException ex) {
            Logger.getLogger(ClientNet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void run() {
        requestFile();
        
        while( true ) {
            try {
                System.out.println("1");

                String filename = fromServer.readUTF();
                System.out.println("2");

                int size = fromServer.readInt();
                System.out.println("3");

                byte[] file = new byte[size];
                System.out.println("4");

                int readSize = fromServer.read( file );

                System.out.println("Where was something to read");

                if( size == readSize ) {
                    writeFile( file );
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            
        }
    }
    
    private void writeFile( byte[] file ) {
        CompletableFuture.runAsync(() -> {
            FileOutputStream outputStream = null;
            try {
                StringJoiner filePath = new StringJoiner( System.getProperty("os.name").toLowerCase().contains("win") ? "\\" : "/" );
                filePath.add( this.clientDir );
                filePath.add( this.filename );
                
                outputStream = new FileOutputStream( filePath.toString() );
                DataOutputStream dataOutStream = new DataOutputStream(new BufferedOutputStream(outputStream));
                dataOutStream.write(file, 0, file.length );
                dataOutStream.close();  
                
            } catch (FileNotFoundException ex) {
                Logger.getLogger(ClientHandler.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ClientHandler.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    outputStream.close();
                } catch (IOException ex) {
                    Logger.getLogger(ClientHandler.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }
    
    public void requestFile() {
        try {
            toServer.writeUTF(Constants.REQUIRE_PREFACE + this.filename);  
            toServer.flush();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ClientHandler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ClientHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}