/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.fileserver.server.controller;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import se.kth.id1212.fbirwe.fileserver.common.AccessDeniedException;
import se.kth.id1212.fbirwe.fileserver.common.ClientCallbackInterface;
import se.kth.id1212.fbirwe.fileserver.common.FileMessage;
import se.kth.id1212.fbirwe.fileserver.common.FileNotFoundException;
import se.kth.id1212.fbirwe.fileserver.common.Fileserver;
import se.kth.id1212.fbirwe.fileserver.common.NotLoggedInException;
import se.kth.id1212.fbirwe.fileserver.common.NotificationType;
import se.kth.id1212.fbirwe.fileserver.common.UserAlreadyLoggedInException;
import se.kth.id1212.fbirwe.fileserver.common.UserAlreadyRegisteredException;
import se.kth.id1212.fbirwe.fileserver.common.UserMessage;
import se.kth.id1212.fbirwe.fileserver.server.integration.FileserverDAO;
import se.kth.id1212.fbirwe.fileserver.server.model.User;
import se.kth.id1212.fbirwe.fileserver.server.net.FileTransmitter;

/**
 *
 * @author fredericbirwe
 */
public class Controller extends UnicastRemoteObject implements Fileserver {
    private final FileserverDAO doa;
    private BiMap<String, UUID> loggedInUsers;
    private HashMap<UUID,ClientCallbackInterface> listeners; 
    private FileTransmitter transmitter;
    
    public Controller() throws RemoteException {
        super();
        this.doa = new FileserverDAO();
        this.loggedInUsers = HashBiMap.create();
        this.listeners = new HashMap<>();
        this.transmitter = new FileTransmitter();
        
        new Thread(this.transmitter).start();
    }   

    
    private void checkUser( UUID userId ) throws NotLoggedInException {
        if( !loggedInUsers.containsValue (userId ) ) {
            throw new NotLoggedInException();
        }
    }
    
    @Override
    public void createUser(String username, String password) throws UserAlreadyRegisteredException {
        User alreadySavedUser = this.doa.getUser(username);
        
        if( alreadySavedUser != null ) {
            throw new UserAlreadyRegisteredException();
        }
        this.doa.addUser( new User(username, password) );
    }

    /**
     *
     * @return
     * @throws RemoteException
     */
    @Override
    public List<String> getAllUsers( UUID userId ) throws NotLoggedInException, RemoteException {
        checkUser( userId );
        
        List<String> users = new ArrayList<>();
        
        for( User u : this.doa.getAllUsers() ) {
            users.add( u.getUsername() );
        }
        
        return users;
    }
    
    @Override
    public UUID login( String username, String pw, ClientCallbackInterface listener ) throws UserAlreadyLoggedInException {
        User u = this.doa.getUser(username);
                
        if ( u != null && u.getPassword().equals(pw) ) {
            if( this.loggedInUsers.containsKey( username )) {
                throw new UserAlreadyLoggedInException();
            }
            
            UUID identifier = UUID.randomUUID();
                    
            this.loggedInUsers.put( username, identifier );
            this.listeners.put(identifier, listener);    
            
            System.out.println( "length: " + this.loggedInUsers.size() );
            
            return identifier;
        } else {
            return null;
        }
    }

    @Override
    public List<? extends FileMessage> getAllFiles( UUID userId ) throws NotLoggedInException, RemoteException {
        checkUser( userId );
        
        return this.doa.getAllFiles();
    }

    @Override
    public void logout( UUID userId ) throws NotLoggedInException, RemoteException {
        checkUser( userId );

        this.loggedInUsers.inverse().remove( userId );
        this.listeners.remove( userId );
    }

    @Override
    public FileMessage readFile( UUID userId, String filename) throws FileNotFoundException, NotLoggedInException, RemoteException {
        checkUser( userId );
        
        FileMessage fileMessage = this.doa.getFile(filename);
             
        if ( fileMessage == null ) {
            throw new FileNotFoundException();
        }
        
        // start download
        //this.transmitter.sendFile
        
        if ( this.loggedInUsers.containsKey( fileMessage.getOwnerName() ) ) {
            ClientCallbackInterface listener = this.listeners.get( this.loggedInUsers.get( fileMessage.getOwnerName() ) );
                        
            // detect reading user
            String readingUser = this.loggedInUsers.inverse().get( userId );
                    
            if( !readingUser.equals( fileMessage.getOwnerName() ) ) {
                listener.notifyClient(NotificationType.READ_FILE, fileMessage, this.doa.getUser(readingUser) );        
            }
        }
        
        return fileMessage;
    }
    
    @Override
    public void writeFile(UUID userId, String filename, String owner, boolean publicFile) throws AccessDeniedException, NotLoggedInException, RemoteException {
        checkUser( userId );
                        
        if ( this.doa.getFile(filename) == null ) {
            this.doa.addFile(filename, owner, publicFile);
        } else {
            
            FileMessage fileMessage = this.doa.getFile( filename );
            
            if ( owner.equals( fileMessage.getOwnerName() ) ) {
                throw new AccessDeniedException();
            }            
        }
        
    }
}
