/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.fileserver.common;

/**
 *
 * @author fredericbirwe
 */
public class Constants {
    public static final String IP = "localhost";
    public static final int PORT = 8084;
    public static final String REQUIRE_PREFACE = "REQUEST:";
    public static final String USER_DIR = "/Users/fredericbirwe/Desktop/Uni_7Semester_KTH/Network Programming/Homework/homework_03/user_dir";
}
